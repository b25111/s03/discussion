# [SECTION] Lists
names = ["John", "Paul", "George", "Ringo"] #List of Strings
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20] # list of numbers
truth_values = [True, False, True, True, False] #Lists of boolean values

#For getting the length of the list
print(len(programs))

# For getting an item from the list
print(programs[0])

# For getting items based on an index range
# The first index must be lower than the second one
print(programs[0:2])

# You can update the value of a specific item in the list by calling the item by the index and assigning a new value to it
print(programs[2])

programs[2] = 'Short Courses'

print(programs[2])


# [SECTION] List Manipulation
#Adds a new item onto the list
durations.append(367)
print(durations)

# Deleting an item from the list
del durations[-1]
print(durations)

# For sorting a list
names.sort()
print(names)

test_list = [1, 3, 5, 7, 9]
test_list.clear()
print(test_list)



# [SECTION] Dictionaries
person1 = {
	"name": "Elon",
	"age": 70,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

# Getting the length of the dictionary
print(len(person1))

# Getting a specific value based on the key
print(person1["name"])

# Getting all the keys in the dictionary
print(person1.keys())

# Getting all the values in the dictionary
print(person1.values())

# Getting all the items along with both their keys and values
print(person1.items())


# [SECTION] Dictionary Manipulation
# Adding a new key-value pair to a dictionary can be done using the update() function
person1["nationality"] = "Norwegian"
person1.update({"fave_food": "Pancit"})
print(person1)

# Deleting an entry from the dictionary can be done either using the 'pop()' function or the 'del' keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# For clearing all of the entries in the dictionary
person2 = {
    "name": "Mystika",
    "age": "18"
}

person2.clear()
print(person2)

# Nested Dictionaries
person3 = {
    "name": "Monika",
    "age": 21,
    "occupation": "Lyricist",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Ruby"]
}

classRoom = {
    "student1": {"name": "Elon",
	"age": 70,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]},
    "student2": person3
}


# [SECTION] Functions
def my_greeting():
    print("Hello User")

my_greeting()

def greet_user(username):
    print(f"Hello,{username}")

greet_user("Elon")

# Lambda Functions
# Lambda Functions are anonymous functions that can execute short code expressions
greeting = lambda person : f"Hello {person}!"
print(greeting("Elsie"))
print(greeting("Tony"))

# [SECTION] Classes
class Car():
    def __init__(self, brand, model, year_of_make):
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make

        # Other properties
        self.fuel = "Gasoline"
        self.fuel_level = 0
        self.distance_traveled = 0

    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}")
        print('...filling up the fuel tank')
        self.fuel_level = 100
        print(f"New fuel level: {self.fuel_level}")

    def drive(self):
        self.distance_traveled = 69
        print(f'Total distance traveled: {self.distance_traveled} km')
        print(f'New fuel level: {self.fuel_level - self.distance_traveled}')

    # MINI ACTIVITY
    # Declare a drive function for the Car class which will print the total disctance that the car has already driven.
    # Along with that, also print the fuel level minus the distance that the car has driven

new_car = Car("Maserati", "Explorer", "2005")

print(f"My car is a {new_car.brand} {new_car.model}")
new_car.fill_fuel()
new_car.drive()